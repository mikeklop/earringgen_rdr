var Order = require("../models/order");
// var Customer = require("../models/customer");

var stripe = require("stripe")("apiXXXX");
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var emailTemplates = require('email-templates');
var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
var templateDir = path.join(__dirname, '..', 'templates');
var template = new EmailTemplate(path.join(templateDir, 'receipt-email'))

var index = function(req, res, next){

  Order.find({}, function(error, orders){
    res.render('orders/index', {orders: orders});
  });
};

var show = function(req, res, next){
  Order.findById(req.params.id, function(error, order){
    if (error) res.json({message: 'Could not find order because ' + error});
    res.render('orders/show', {order: order});
  });
};

var createInvoice = function(req, res){

  var email = req.body.email;
  var orderDescription = req.body.orderDescription;
  var price = req.body.price/100;

  var envelope = {
    debug: true,
    host: 'smtp.gmail.com',
    port: 465,
    auth: {
    user: 'not available',
    pass: 'xxxxxxx'
    },
  	maxConnections: 5,
  	maxMessages: 10
  };

  var transporter = nodemailer.createTransport(smtpTransport(envelope));

         var locals = {
        email: req.body.email,
        name: req.body.firstName,
        description: orderDescription,
        price: price
      };

    template.render(locals, function (err, results) {
    if (err) {
      return console.error(err);
    }

    transporter.sendMail({
      from: 'Rebecca de Ravenel <admin@rebeccaderavenel.com>',
      to: locals.email,
      subject: 'Thanks for your Custom Order',
      html: results.html,
      text: results.text
    }, function (err, responseStatus) {
      if (err) {
        console.error(err);
      }
      else {
  	    console.log(responseStatus.message);
      }
    });

  });

};

var create = function(req, res) {

	  var order       = new Order();
	  var stripeToken = req.body.stripeToken;
	  order.firstName      = req.body.firstName;
	  order.lastName  = req.body.lastName;
	  order.company   = req.body.company;
	  order.email = req.body.email;
	  order.phone = req.body.phone;
	  order.date = req.body.date;
	  order.streetAddress1 = req.body.streetAddress1;
	  order.streetAddress2 = req.body.streetAddress2;
	  order.zipCode = req.body.zipCode;
	  order.state = req.body.state;
	  order.country = req.body.country;
	  order.city = req.body.city;
	  order.description = req.body.orderDescription;
	  order.giftMessage = req.body.giftMessage;
	  order.price = req.body.price;

   var destination = order.firstName + " " + order.lastName + ", " + order.streetAddress1 + ", " + order.streetAddress2 + ", " +
   order.city + ", " + order.zipCode + ", " + order.state + ", " + order.country;


   var billingAdd = req.body.billingAddress1 + ", " + req.body.billingAddress2 + ", " + req.body.billingCity + ", " + req.body.billingState + ", "
   + req.body.billingZipCode + ", " + req.body.billingCountry;

	 stripeToken.card.address_city = req.body.city;
	 stripeToken.card.address_country = req.body.address_country;
	 stripeToken.card.address_line1 = req.body.streetAddress1;
	 stripeToken.card.address_line1_check = req.body.streetAddress2;
	 stripeToken.card.address_zip_check = req.body.zipCode;
	 stripeToken.card.address_state = req.body.state;
	 stripeToken.card.address_zip = req.body.zipCode;
	 stripeToken.card.country = req.body.country;
	 stripeToken.card.name = order.firstName + order.lastName;


	var createCustomer = stripe.customers.create({
		  description: 'Customer for rdr',
		  source: stripeToken.id,
		  email: req.body.email
		},
		function(err, customer) {
        if (err){
                console.log(err);
                res.json({error: err});
              }
        else if (err && err.type === 'card_error') {
          res.json({error: err});
          return;
        }
		    else {
			      console.log(customer);
			      var customerId = customer.id;
				    var charge = stripe.charges.create({
					  amount: order.price, // amount in cents, again
					  currency: "usd",
					  customer:  customerId,
					  receipt_email: order.email,
					  description: "CUSTOM EARRINGS. ITEMS WILL SHIP DECEMBER 1st. ITEMS ARE NON-RETURNABLE.",
					  metadata: {'order_id': order.lastName + order.date, 'shipping address': destination, 'order description': order.description, 'gift message': order.giftMessage, 'billing address': billingAdd, 'phone': order.phone}
					},
  					function(err, charge) {
               if (err){
                  console.log(err);
                  res.json({error: err});
                }
                else if (err && err.type === 'card_error') {
                  // The card has been declined
                  res.json({error: err});
                  return;
                }
                else {
                   console.log("hey it went through!");
                    order.save(function(err, savedOrder) {
                      if (err) {
                       res.json({error: err});
                      }
                      else {
                        res.json(savedOrder);
                      }
                  });

              }

  				});
		    }
		});

};



module.exports = {
  index: index,
  show:  show,
  create: create,
  createInvoice: createInvoice
};
