	var express = require('express'),
    router  = new express.Router();

// Require controllers.
var welcomeController = require('../controllers/welcome');
var ordersController   = require('../controllers/orders');


//routes
router.get('/', welcomeController.index);
router.post('/orders', ordersController.create);
router.post('/createInvoice', ordersController.createInvoice);
router.get('/orders', ordersController.index);


module.exports = router;
