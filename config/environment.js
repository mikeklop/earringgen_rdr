var _ = require('lodash');

var localEnvVars = {
  TITLE:      'EarringGen',
  SAFE_TITLE: 'EarringGen'
};

// Merge all environmental variables into one object.
module.exports = _.extend(process.env, localEnvVars);
