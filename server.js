var express      = require('express');
var path         = require('path');
var favicon      = require('serve-favicon');
var logger       = require('morgan');
var bodyParser   = require('body-parser');
var debug        = require('debug')('app:http');
var cookieParser = require('cookie-parser');

//local loads
var env      = require('./config/environment'),
    mongoose = require('./config/database'),
    routes   = require('./config/routes');

require('dotenv').load();

var app = express();

// Configuring
app.set('title', env.TITLE);
app.set('safe-title', env.SAFE_TITLE);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.locals.title = app.get('title');

// Logging layer.
app.use(logger('dev'));

// Helper layer (parses the requests, and adds further data).
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser('notsosecretnowareyou'));


// Routes to static assets.
app.use(express.static(path.join(__dirname, 'public')));

app.use(debugReq);

// Defines all of our "dynamic" routes.
app.use('/', routes);

// 404 Catching
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// 500 Error-handling layer.
app.use(function(err, req, res, next) {
  err = (app.get('env') === 'development') ? err : {};
  res.status(err.status || 500);

});

function debugReq(req, res, next) {
  debug('params:', req.params);
  debug('query:',  req.query);
  debug('body:',   req.body);
  next();
}

module.exports = app;
