var mongoose = require('mongoose'),
    debug    = require('debug')('app:models');

var orderSchema = new mongoose.Schema({
  name:   String,
  handle: String,
  firstName: String,
  lastName: String,
  company: String,
  email: String,
  phone: String,
  date: String,
  streetAddress1: String,
  streetAddress2: String,
  zipCode: Number,
  state: String,
  country: String,
  city: String,
  description: String,
  giftMessage: String,
  price: Number

});


var Order = mongoose.model('Order', orderSchema);

module.exports = Order;
