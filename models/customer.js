var mongoose = require('mongoose'),
    debug    = require('debug')('app:models');

var customerSchema = new mongoose.Schema({
  name: String,
  customerId: String
});

var Customer = mongoose.model('Customer', customerSchema);

module.exports = Customer;
