// console.log('JS loaded!');

$(document).ready(function($) {

	$('#carouselHacked').carousel({
        interval: 6000
    });


	var isOpen = false;


		 $(".dropOneTitle").click(function(){

				 	//SWAP CLASSES FOR OTHER DROPDOWNS//
				 	$(".dropDown2").removeClass("fullSize");
				 	$(".dropDown3").removeClass("fullSize");
				 	$(".dropDown4").removeClass("fullSize");
					$(".dropTwoTitle").removeClass("active");
					$(".dropThreeTitle").removeClass("active")
					$(".dropFourTitle").removeClass("active")
					$(".drop2Direction").remove("fa-angle-up").addClass("fa-angle-down");
					$(".drop3Direction").remove("fa-angle-up").addClass("fa-angle-down");
					$(".drop4Direction").remove("fa-angle-up").addClass("fa-angle-down");
				 	$(".openclose2").html("OPEN");
				 	$(".openclose3").html("OPEN");
				 	$(".openclose4").html("OPEN");


				 	if ($(".dropOneTitle").hasClass("active")){
				 		$(".dropDown1").removeClass("fullSize");
				 		$(".dropOneTitle").removeClass("active")
				 		$(".openclose1").html("OPEN");
				 		isOpen = false;
				 		$(".drop1Direction").removeClass("fa-angle-up");
				 		$(".drop1Direction").addClass("fa-angle-down");

				 	}
				 	else {
					 	$(".dropDown1").addClass("fullSize");
					 	$(".dropOneTitle").addClass("active");
					 	$(".openclose1").html("CLOSE");
					 	$(".drop1Direction").addClass("fa-angle-up");
				 		$(".drop1Direction").removeClass("fa-angle-down");

					 	isOpen = true;
					}

		 });

		$(".dropTwoTitle").click(function(){


				 //SWAP CLASSES FOR OTHER DROPDOWNS//
				 	$(".dropDown1").removeClass("fullSize");
				 	$(".dropDown3").removeClass("fullSize");
				 	$(".dropDown4").removeClass("fullSize");
					$(".dropOneTitle").removeClass("active");
					$(".dropThreeTitle").removeClass("active")
					$(".dropFourTitle").removeClass("active")
					$(".drop1Direction").remove("fa-angle-up").addClass("fa-angle-down");
					$(".drop3Direction").remove("fa-angle-up").addClass("fa-angle-down");
					$(".drop4Direction").remove("fa-angle-up").addClass("fa-angle-down");
					$(".openclose1").html("OPEN");
				 	$(".openclose3").html("OPEN");
				 	$(".openclose4").html("OPEN");


					if ($(".dropTwoTitle").hasClass("active")){
				 		$(".dropDown2").removeClass("fullSize");
				 		$(".dropTwoTitle").removeClass("active")
				 		$(".openclose2").html("OPEN");
				 		isOpen = false;
				 		$(".drop2Direction").removeClass("fa-angle-up");
				 		$(".drop2Direction").addClass("fa-angle-down");
				 	}
				 	else {
					 	$(".dropDown2").addClass("fullSize");
					 	$(".dropTwoTitle").addClass("active");
					 	$(".openclose2").html("CLOSE");
					 	$(".drop2Direction").addClass("fa-angle-up");
				 		$(".drop2Direction").removeClass("fa-angle-down");
					 	isOpen = true;
					 }
		 });
		$(".dropThreeTitle").click(function(){
				 	//SWAP CLASSES FOR OTHER DROPDOWNS//
				 	$(".dropDown1").removeClass("fullSize");
				 	$(".dropDown2").removeClass("fullSize");
				 	$(".dropDown4").removeClass("fullSize");
					$(".dropOneTitle").removeClass("active");
					$(".dropTwoTitle").removeClass("active");
					$(".dropFourTitle").removeClass("active");
					$(".drop1Direction").remove("fa-angle-up").addClass("fa-angle-down");
					$(".drop2Direction").remove("fa-angle-up").addClass("fa-angle-down");
					$(".drop4Direction").remove("fa-angle-up").addClass("fa-angle-down");
				  	$(".openclose1").html("OPEN");
				 	$(".openclose2").html("OPEN");
				 	$(".openclose4").html("OPEN");



					if ($(".dropThreeTitle").hasClass("active")){
				 		$(".dropDown3").removeClass("fullSize");
				 		$(".dropThreeTitle").removeClass("active")
				 		$(".openclose3").html("OPEN");
				 		isOpen = false;
				 		$(".drop3Direction").removeClass("fa-angle-up");
				 		$(".drop3Direction").addClass("fa-angle-down");
				 	}
				 	else {
					 	$(".dropDown3").addClass("fullSize");
					 	$(".dropThreeTitle").addClass("active");
					 	$(".openclose3").html("CLOSE");
					 	isOpen = true;
					 	$(".drop3Direction").addClass("fa-angle-up");
				 		$(".drop3Direction").removeClass("fa-angle-down");
				 	}
		 });

		$(".dropFourTitle").click(function(){
				 	//SWAP CLASSES FOR OTHER DROPDOWNS//
				 	$(".dropDown1").removeClass("fullSize");
				 	$(".dropDown2").removeClass("fullSize");
				 	$(".dropDown3").removeClass("fullSize");
					$(".dropOneTitle").removeClass("active");
					$(".dropTwoTitle").removeClass("active")
					$(".dropThreeTitle").removeClass("active")
					$(".drop1Direction").remove("fa-angle-up").addClass("fa-angle-down");
					$(".drop2Direction").remove("fa-angle-up").addClass("fa-angle-down");
					$(".drop3Direction").remove("fa-angle-up").addClass("fa-angle-down");
					$(".openclose1").html("OPEN");
				 	$(".openclose2").html("OPEN");
				 	$(".openclose3").html("OPEN");


					if ($(".dropFourTitle").hasClass("active")){
				 		$(".dropDown4").removeClass("fullSize");
				 		$(".dropFourTitle").removeClass("active")
				 		$(".openclose4").html("OPEN");
				 		isOpen = false;
				 		$(".drop4Direction").removeClass("fa-angle-up");
				 		$(".drop4Direction").addClass("fa-angle-down");

				 	}
				 	else {
					 	$(".dropDown4").addClass("fullSize");
					 	$(".dropFourTitle").addClass("active");
					 	$(".openclose4").html("CLOSE");
					 	$(".drop4Direction").addClass("fa-angle-up");
				 		$(".drop4Direction").removeClass("fa-angle-down");
					 	isOpen = true;
				 	}
		 });




		 $(".payCard").click(function(){

		 	$(".specialForm").removeClass("hideForm").addClass("showForm");
		 	$(".payCard").hide();

		 });


		$(function() {


		  var $form = $('#payment-form');
			  $form.submit(function(event) {
			    // Disable the submit button to prevent repeated clicks:
			    $form.find('.submit').prop('disabled', true);

			    // Request a token from Stripe:
			    Stripe.card.createToken($form, stripeResponseHandler);

			    // Prevent the form from being submitted:
			    return false;
			  });

			function stripeResponseHandler(status, response) {
			  // Grab the form:
			  var $form = $('#payment-form');

			  if (response.error) { // Problem!

			    // Show the errors on the form:
			    $form.find('.payment-errors').text(response.error.message);
			    $form.find('.submit').prop('disabled', false); // Re-enable submission

			  } else {
			  // Token was created!

			    // Get the token ID:
			    var token = response.id;

			    // Insert the token ID into the form so it gets submitted to the server:
			    $form.append($('<input type="hidden" name="stripeToken">').val(token));

			    // Submit the form:
			    $form.get(0).submit();
			  }
			};

		});


});

