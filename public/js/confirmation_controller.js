(function() {
  "use strict";

  angular
    .module("earrGen")
    .controller("ConfirmationController", ConfirmationController)
    .run(function($rootScope){
      $rootScope.$on('$stateChangeSuccess',function(){
          $("html, body").animate({ scrollTop: 0 }, 200);
      });
 });

  ConfirmationController.$inject = ["$scope", "$state", "$location", "$log", "$rootScope", "$window", "$timeout", "$http", "UserOrder", "$localStorage"];

   function ConfirmationController($scope, $state, $location, $log, $rootScope, $window, $timeout, $http,  UserOrder, $localStorage) {

          var colors = [];
          var topColors = [];
          var selected1, selected2, selected3, selected4;

          var newQuantity;
          var i = 0;
          $scope.quantity = 1;

          $scope.yourEmail = UserOrder.email;

           $http.post('/createInvoice', {
                      firstName: $localStorage.name,
                      lastName: UserOrder.lastName,
                      orderDescription: "Custom Earrings: Top Drop - " + $localStorage.one + ", Drop 2 - " + $localStorage.two +
                    ", Drop 3 - " + $localStorage.three + ", Drop 4 - " + $localStorage.four,
                      email: UserOrder.email,
                      price: UserOrder.price
           });


          if ($localStorage.one){
            $scope.topCircleColor ="../assets/Tops/"+ $localStorage.oneClass + ".png";
            $scope.secondCircleColor = "../assets/balls/"+ $localStorage.twoClass + ".png";
            $scope.thirdCircleColor = "../assets/balls/"+ $localStorage.threeClass + ".png";
            $scope.fourthCircleColor = "../assets/balls/"+ $localStorage.fourClass + ".png";

           $scope.quantity = UserOrder.getQuantity();

           $scope.firstTotal = $scope.quantity * 325;
           $scope.result = $scope.firstTotal + 17;

          }

          $scope.colorOne = $localStorage.one;
          $scope.colorTwo = $localStorage.two;
          $scope.colorThree = $localStorage.three;
          $scope.colorFour = $localStorage.four;


      };

})();



