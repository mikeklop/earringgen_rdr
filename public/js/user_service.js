(function() {
  "use strict";

    angular
    .module("earrGen")
    .factory("UserOrder", UserOrder);

 UserOrder.$inject = ["$window", "$localStorage"];


function UserOrder($window, $localStorage) {

    var orderFactory = {};
    orderFactory.colors = {};
    orderFactory.orderOne = [];
    $localStorage.orderNum = 0;

    $localStorage.quantity = 1;
    orderFactory.quantity = 1;

    orderFactory.setOrder = function(){
      $localStorage.orderNum = 1;
    }

    orderFactory.setQuantity = function(newQuantity){
      $localStorage.quantity = newQuantity;
      orderFactory.quantity = $localStorage.quantity;
    }

    orderFactory.getQuantity = function(){
      return orderFactory.quantity;
    }


     orderFactory.setColors = function(colors) {

       $localStorage.colors = colors;
       orderFactory.colors = $localStorage.colors;

       console.log("colors:" + orderFactory.colors);
    }



    orderFactory.getColors= function() {
        return orderFactory.colors;
      // return $window.localStorage.getItem('colors');
    };

    return orderFactory;


}


})();
