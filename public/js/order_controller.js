(function() {
  "use strict";

  angular
    .module("earrGen")
    .controller("OrderController", OrderController)
    .config(function($windowProvider) {
      var $window = $windowProvider.$get();
      $window.Stripe.setPublishableKey('pk_live_stubztnbS8UGsJs5jMvRIKRJ');
  });

  OrderController.$inject = ["$scope", "$state", "$location", "$log", "$rootScope", "$window", "$timeout", "$http", "UserOrder", "$localStorage"];

   function OrderController($scope, $state, $location, $log, $rootScope, $window, $timeout, $http, UserOrder, $localStorage) {

          var newQuantity;
          var i = 0;
          $scope.quantity = 1;

           console.log(localStorage.oneClass);

           var item1 = "/assets/Tops/"+ $localStorage.oneClass + ".png";
           var item2 = "/assets/balls/"+ $localStorage.twoClass + ".png";
           var item3 = "/assets/balls/"+ $localStorage.threeClass + ".png";
           var item4 = "/assets/balls/"+ $localStorage.fourClass + ".png";

            $scope.topCircleColor = item1;
            $scope.secondCircleColor = item2;
            $scope.thirdCircleColor = item3;
            $scope.fourthCircleColor = item4;


          if ($localStorage.oneClass){

           $scope.quantity = UserOrder.getQuantity();

           $scope.firstTotal = $scope.quantity * 325;
           $scope.result = $scope.firstTotal + 17;

          }

          $scope.colorOne = $localStorage.one;
          $scope.colorTwo = $localStorage.two;
          $scope.colorThree = $localStorage.three;
          $scope.colorFour = $localStorage.four;


          UserOrder.setOrder();

          $localStorage.design = "created";

          $scope.editOrder= function(form){
             // console.log(form.quantity);
            newQuantity = form.quantity.$viewValue;
            UserOrder.setQuantity(newQuantity);
            $state.go("landing");
         }

         $scope.changeQuantity = function(form){
             $scope.firstTotal = form.quantity.$viewValue * 325;
          $scope.result = $scope.firstTotal + 17;
          }

          // Stripe Response Handler
          $scope.stripeCallback = function (code, result) {
              if (result.error) {
                $scope.response = "Please check credit card info and ensure all fields are filled out correctly."
                console.log(result.error);
              } else {
                  submitThis(result);
              }
          };

          function submitThis(result){
            var currentDate = new Date();
            var price = $scope.result * 100;
            var firstName = $scope.firstName;
            var lastName = $scope.lastName;
            var email = $scope.email;
            UserOrder.email = $scope.email;
            UserOrder.firstName = $scope.firstName;
            UserOrder.price = price;
            $localStorage.name = $scope.firstName;

               $http({
                      url: '/orders',
                      method: 'POST',
                      data: {
                        firstName: $scope.firstName,
                        lastName: $scope.lastName,
                        email: $scope.email,
                        phone: $scope.phone,
                        streetAddress1: $scope.shippingAddress1,
                        streetAddress2: $scope.shippingAddress2,
                        zipCode: $scope.shippingzipcode,
                        state: $scope.shippingState,
                        country: "United States",
                        city: $scope.shippingCity,
                        orderDescription: "Top Drop:" + $scope.colorOne + ", Drop 2:" + $scope.colorTwo +
                        ", Drop 3:" + $scope.colorThree + ", Drop 4:" + $scope.colorFour,
                        giftMessage: $scope.giftMessage,
                        stripeToken: result,
                        date: currentDate,
                        price: price,
                        billingAddress1: $scope.billingAddress1,
                        billingAddress2: $scope.billingAddress2,
                        billingCity: $scope.billingCity,
                        billingState: $scope.billingState,
                        billingZipCode: $scope.billingzipcode,
                        billingCountry: $scope.billingCountry
                      },
                      headers: {'Content-Type': 'application/json'}
                })
                .success(
                  function(response){
                    if(response.error){
                      if (response.error.code == "card_declined"){
                        console.log(response.error);
                        $scope.response = "Your card was declined.";
                        return;
                       }
                       else if (response.error.code == "processing_error"){
                        console.log(response.error);
                        $scope.response = "Processing error. Please check your card and try again.";
                        return;
                       }
                       else if(response.error.code == "incorrect_cvc"){
                         $scope.response = "Incorrect CVC Code. Please check your card and try again."
                        return;
                       }
                       else if(response.error.code == "expired_card"){
                         $scope.response = "Card is expired. Please check the date and try again.";
                       }
                    }
                  else {
                      $state.go("confirmation");
                     }
                });
            }

          $scope.$watch(
            function($scope) {
             $scope.firstTotal = $scope.quantity * 325;
             $scope.result = $scope.firstTotal + 17;
          });

          angular.element($window).on('resize', function(){
           $scope.firstTotal = $scope.quantity * 325;
           $scope.result = $scope.firstTotal + 17;
           $scope.$apply();
          })

      };

})();



