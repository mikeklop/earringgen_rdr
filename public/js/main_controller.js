(function() {
  "use strict";

  angular
    .module("earrGen")
    .controller("MainController", MainController);

  MainController.$inject = ["$scope", "$state", "$location", "$rootScope", "$window", "$timeout", "$http", "UserOrder", "$localStorage" ];

   function MainController($scope, $state, $location, $rootScope, $window, $timeout, $http, UserOrder, $localStorage) {

          var colors = [];
          var topColors = [];
          $scope.allColors = [];
          var newQuantity;
          $scope.quantity = $localStorage.quantity;


          $scope.topColors = [{name: "Light Gold", cssClass: "TopDrop_LightGold"}, {name: "Canary", cssClass: "TopDrop_Canary"},
          {name: "Peach", cssClass: "TopDrop_Peach"}, {name: "Apricot", cssClass: "TopDrop_Apricot"}, {name: "Coral",
          cssClass: "TopDrop_Coral"}, {name: "Hot Pink", cssClass: "TopDrop_HotPink"}, {name: "Red", cssClass: "TopDrop_Red"},
          {name: "Fuchsia", cssClass: "TopDrop_Fuchsia"}, {name: "Cobalt", cssClass: "TopDrop_Cobalt"},
          {name: "Sky Blue", cssClass: "TopDrop_SkyBlue"}, {name: "Baby Blue", cssClass: "TopDrop_BabyBlue"},
          {name: "Silver", cssClass: "TopDrop_Silver"}];

          $scope.colors = [{name: "Light Gold", cssClass: "LightGold"}, {name: "Canary", cssClass: "Canary"}, {name: "Peach", cssClass: "Peach"},
          {name: "Apricot", cssClass: "Apricot"}, {name: "Coral", cssClass: "Coral"}, {name: "Hot Pink", cssClass: "HotPink"},
          {name: "Red", cssClass: "Red"}, {name: "Fuchsia", cssClass: "Fuchsia"}, {name: "Cobalt", cssClass: "Cobalt"},
          {name: "Sky Blue", cssClass: "SkyBlue"}, {name: "Baby Blue", cssClass: "BabyBlue"}, {name: "Silver", cssClass: "Silver"}];


          $scope.CreateOrder= function(){
            $state.go("form");
          }

          var i=0;

        var colorOne, colorTwo, colorThree, colorFour;

        if ($localStorage.orderNum == 1){

            $scope.quantity = $localStorage.quantity;

            $scope.topCircleColor = "/../assets/Tops/"+ $localStorage.oneClass + ".png";
            $scope.small1 = $localStorage.oneClass;

            $scope.secondCircleColor = "/../assets/balls/"+ $localStorage.twoClass + ".png";
            // $scope.secondCircleColor =  $localStorage.;
            $scope.small2 = $localStorage.twoClass;

            $scope.thirdCircleColor = "/../assets/balls/"+ $localStorage.threeClass + ".png";
            $scope.small3 = $localStorage.threeClass;

            $scope.fourthCircleColor = "/../assets/balls/"+ $localStorage.fourClass + ".png";
            $scope.small4 = $localStorage.fourClass;

          }


          var newColors = {};
          $localStorage.orderNum =  [];

           $scope.selectColor = function(colorName, cssClass){
            $scope.Selected1 = colorName;
            var selected1 = colorName;
            $scope.topCircleColor = "../assets/Tops/"+ cssClass + ".png";
            $scope.small1 = cssClass;

            $localStorage.one = selected1;
            $localStorage.oneClass = cssClass;

            newQuantity = $scope.quantity;
            UserOrder.setQuantity(newQuantity);
           }

          $scope.selectColor2 = function(colorName, cssClass){
            $scope.Selected2 = colorName;
            var selected2 = colorName;
            $scope.secondCircleColor = "assets/balls/"+ cssClass + ".png";
            $scope.small2 = cssClass;
            $localStorage.two = selected2;
            $localStorage.twoClass = cssClass;
            newQuantity = $scope.quantity;
            UserOrder.setQuantity(newQuantity);
           }

          $scope.selectColor3 = function(colorName, cssClass){
              $scope.Selected3 = colorName;
              var selected3 = colorName;
              $scope.thirdCircleColor = "assets/balls/"+ cssClass + ".png";
              $scope.small3 = cssClass;
              $localStorage.three = selected3;
              $localStorage.threeClass = cssClass;
              newQuantity = $scope.quantity;

              UserOrder.setQuantity(newQuantity);

           }

          $scope.selectColor4 = function(colorName, cssClass){
            $scope.Selected4 = colorName;
            var selected4 = colorName;
            console.log(colorName);
            $scope.fourthCircleColor = "assets/balls/"+ cssClass + ".png";
            $scope.small4 = cssClass;
            $localStorage.four = selected4;
            $localStorage.fourClass = cssClass;
            newQuantity = $scope.quantity;

            UserOrder.setQuantity(newQuantity);
           }

         var tests = [];

          function areAllEqual( arr )
            {
               for( var i = 0; i < arr.length - 1; i++ )
               {
                  if( arr[ i ] != arr[ i + 1 ] ) return false;
               }
               return true;
            }


           $scope.checkout= function(form){
              newQuantity = form.quantity.$viewValue;
              UserOrder.setQuantity(newQuantity);


             $state.go("form");

          }

          $scope.checkSelections = function(){
              tests = [
                 $localStorage.one,
                 $localStorage.two,
                 $localStorage.three,
                 $localStorage.four
                  ];
              if (areAllEqual(tests)) { // your question said "more than one element"
                 $scope.response = "Make sure you select at least two colors please";
                 $scope.responseClass = "errorMessage2"
                 return true;
              }
              else if ($localStorage.one == undefined || $localStorage.two == undefined || $localStorage.four == undefined || $localStorage.four == undefined){
                 $scope.response = "Please fill in all the drops";
                 $scope.responseClass = "errorMessage2"
                 return true;
              }
                else {
                 $scope.response = "";
                 $scope.responseClass = ""
                 return false;
                }
          };



      };

})();



