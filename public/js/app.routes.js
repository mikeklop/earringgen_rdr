(function(){
  "use strict";

  angular
    .module("earrGen")
    .config(AppRoutes);


  AppRoutes.$inject = ["$stateProvider", "$locationProvider", "$urlRouterProvider"];

  function AppRoutes($stateProvider, $locationProvider, $urlRouterProvider){

    $stateProvider
      .state("landing", {
        url: "/",
        templateUrl: "/templates/base.html"
      })
      .state("form", {
        url: "/form",
        templateUrl: "/templates/payment_form.html"
      })
      .state("confirmation", {
        url: "/confirmation",
        templateUrl: "/templates/confirmation.html"
      });

      $urlRouterProvider.otherwise("/");
      // $locationProvider.html5Mode(true);

}

})();
