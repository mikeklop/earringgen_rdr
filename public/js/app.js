(function() {

  angular
    .module("earrGen", [
      'ui.router', 'ngAnimate', 'ngMessages', 'angularPayments', 'ngStorage']);
})();
